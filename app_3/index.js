"use strict";

const express = require("express");
const app = express();

app.use(express.static(__dirname + "/static"));

app.get('/*', function(req, res) {
    res.sendfile("static/api.html");
    console.log("===> app_3");
});

const port = process.env.PORT || 5030;
app.listen(port);
console.log("Server works on port " + port);
